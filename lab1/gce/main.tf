resource "google_compute_instance" "instance" {
  name         = "instance-${count.index}"
  machine_type = "${var.machine_type}"
  zone         = "${var.zone}"
  tags         = ["${var.tags}"]

  boot_disk {
  initialize_params {
   image = "${var.image}"
  }
  }

  network_interface {
    network = "${var.network}"

    access_config {
      # Ephemeral
    }
  }

  provisioner "file" {
    connection {
      type    = "ssh"
      user    = "${var.user}"
      private_key = "${file(var.gce_ssh_pri_key_file)}"
      timeout = "30s"
      agent   = "false"
    }

    source      = "${var.source}"
    destination = "${var.destination}"
  }

  provisioner "remote-exec" {
    connection {
      type    = "ssh"
      user    = "${var.user}"
      private_key = "${file(var.gce_ssh_pri_key_file)}"
      timeout = "30s"
      agent   = "false"
    }

    inline = [
      "sudo chmod +x ${var.destination}/script.sh",
      "sudo chown root.root ${var.destination}/script.sh",
      "sudo ${var.destination}/script.sh",
      "sudo chmod +x ${var.destination}/installs.sh",
      "sudo chown root.root ${var.destination}/installs.sh",
      "sudo ${var.destination}/installs.sh"
    ]
  }


  service_account {
    scopes = ["cloud-platform"]
  }
  metadata {
    sshKeys = "${var.gce_ssh_user}:${file(var.gce_ssh_pub_key_file)}"
  }
}

