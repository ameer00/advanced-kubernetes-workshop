module "project" {
 source      = "./project"
 credentials = "${file("${var.credentials}")}"
 project     = "${var.project}"
 region      = "${var.region}"
}

module "gke-central" {
  source = "./gkeplain"
  name   = "gke-central"
}

module "gke-east" {
  source = "./gkeplain"
  name   = "gke-east"
  zone   = "us-east1-b"
}

module "gke-spinnaker" {
  source = "./gkeplain"
  name   = "gke-spinnaker"
}

module "fw-ssh" {
  source  = "./firewall"
}

module "gce-instance" {
  source  = "./gce"
  user = "ameerabbas" 
  tags = ["${module.fw-ssh.target_tags}", "${lower("${substr("${module.gke-east.client_certificate}", 0, 5)}")}", "${lower("${substr("${module.gke-central.client_certificate}", 0, 5)}")}", "${lower("${substr("${module.gke-spinnaker.client_certificate}", 0, 5)}")}"]
}
